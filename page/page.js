const emailReg = /^([\s]{0,})((([^<>()\[\]\\.,;:\s@a-zA-Za-яА-Я"]|[a-zA-Z])+(\.([^<>()\[\]\\.,;:\s@"a-zA-Za-яА-Я]|[a-zA-Z])+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))([\s]{0,})$/;
const phoneReg = /^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{5,10}$/;

class Page {
  hostElemAsForm;
  formClass;
  inputWithFriends;
  friendsWrapperElem;
  friendsListElem;
  nameFriendElem;
  btnAddNameFriend;
  friendsGroups = [];
  passwordElem;
  repeatPasswordElem;
  btnReset;

  constructor() {
    this.hostElemAsForm = document.querySelector('#form-wrapper');
console.log(this.hostElemAsForm)
    this.inputWithFriends = this.hostElemAsForm.elements['withFriends'];
    this.friendsWrapperElem = this.hostElemAsForm.querySelector('#friends-wrapper');
    this.friendsListElem = this.hostElemAsForm.querySelector('#friends-list');
    this.nameFriendElem = this.hostElemAsForm.querySelector('.ffg-host-nameFriend');
    this.btnAddNameFriend = this.hostElemAsForm.querySelector('#btn-add-nameFriend');
    this.passwordElem = this.hostElemAsForm.elements['password'];
    this.repeatPasswordElem = this.hostElemAsForm.elements['repeatPassword'];
    this.btnReset = this.hostElemAsForm.querySelector('#pg-btn-reset');

    let formObj = localStorage.getItem('key-form');
    if (formObj) {
      formObj = JSON.parse(formObj);
      console.log(formObj)
    }

    this.formClass = new FormClass(this.hostElemAsForm, [
      {
        name: 'lastName', // фамилия
        validations: {
          required: {
            param: true,
          },
          minLength: {
            param: 3,
            message: 'Не менее 3-х символов'
          },
          maxLength: {
            param: 15,
            message: 'Не более 15-ти символов'
          },
        },
        value: formObj ? formObj.lastName : ''
      },
      {
        name: 'name', // имя
        validations: {
          required: {
            param: true,
          },
          minLength: {
            param: 3,
            message: 'Не менее 3-х символов'
          },
          maxLength: {
            param: 15,
            message: 'Не более 15-ти символов'
          },
        },
        value: formObj ? formObj.name : ''
      },
      {
        name: 'patronymic', // отчество
        validations: {
          minLength: {
            param: 3,
            message: 'Не менее 3-х символов'
          },
          maxLength: {
            param: 15,
            message: 'Не более 15-ти символов'
          },
        },
        value: formObj ? formObj.patronymic : ''
      },
      {
        name: 'dateBirth', // др
        validations: {
          required: {
            param: true,
          },
          minDate: {
            param: {
              date: new Date(1990, 0, 0)
            },
            message: 'Вы родились до 1990 года!'
          },
          maxDate: {
            param: {
              year: -18,
            },
            message: 'Вам ещё нет 18 лет!'
          }
        },
        value: formObj ? formObj.dateBirth : ''
      },
      {
        name: 'phone', // телефон
        validations: {
          required: {
            param: true,
          },
          minLength: {
            param: 11,
            message: '11-значный формат номера'
          },
          maxLength: {
            param: 16,
            message: '11-значный формат номера'
          },
          pattern: {
            param: phoneReg,
            message: 'Только цифры!'
          }
        },
        value: formObj ? formObj.phone : ''
      },
      {
        name: 'email', // почта
        validations: {
          required: {
            param: true,
          },
          pattern: {
            param: emailReg,
            message: 'Должен быть "@"'
          }
        },
        value: formObj ? formObj.email : ''
      },
      {
        name: 'city', // город-селектор
        validations: {
          required: {
            param: true,
          },
        },
        value: formObj ? formObj.city : ''
      },
      {
        name: 'beach', // пляж-радио
        validations: {
          required: {
            param: true,
            message: 'Выберете пляж'
          },
        },
        value: formObj ? formObj.beach : ''
      },
      {
        name: 'hireThings', // прокат-чек
        value: formObj ? formObj.hireThings : ''
      },
      {
        name: 'food', // еда-чек
        validations: {
          required: {
            param: true,
            message: 'Выберете еду'
          },
          minLength: {
            param: 2,
            message: 'Не менее 2-х блюд'
          },
        },
        value: formObj ? formObj.food : ''
      },
      {
        name: 'password', // Пароль
        validations: {
          required: {
            param: true,
          },
          minLength: {
            param: 3,
            message: 'Не менее 3-ти символов'
          }
        },
        value: formObj ? formObj.password : ''
      },
      {
        name: 'repeatPassword', // Повторный пароль
        validations: {
          custom: [
            {
              param: this.validDoublePassword.bind(this),
              message: 'Пароли не совпадают '
            },
          ]
        },
        value: formObj ? formObj.repeatPassword : ''
      }
    ]);

    this.hostElemAsForm.onsubmit = event => { // в момент отправки формы:
      event.preventDefault();
      this.formClass.markAsShowAllGroups()

      let isValid = true;
      this.friendsGroups.forEach(group => {
        if (!group.makeValidation()) {
          isValid = false;
        }
      });
      if (!this.formClass.makeValidationAllGroups()) {
        isValid = false;
      }
      if (isValid) {
        const formModel = {};
        Object.keys(this.formClass.groups).forEach(key => {
          const group = this.formClass.groups[key];
          const value = group.getValue();

          if ((Object.prototype.toString.call(value) !== '[object Array]' && value !== '') ||
              (Object.prototype.toString.call(value) === '[object Array]' && value.length)) {
            formModel[key] = group.getValue();
          }
        });

        const arrFriends = [];
        this.friendsGroups.forEach(key => {
          arrFriends.push(key.getValue());
          formModel['friends'] = arrFriends;
        });

        localStorage.setItem('key-form', JSON.stringify(formModel))
      }
    }

    this.btnReset.onclick = () => {
      this.formClass.resetShowAllGroups()
    }

    this.inputWithFriends.onclick = () => {
      this.toggleShowFriends();
    }

    this.btnAddNameFriend.onclick = () => {
      this.addFriend();
    }
  }

  toggleShowFriends() {
    setTimeout(() => {
      if (this.inputWithFriends.getAttribute('checked')) {
        if (!this.friendsGroups.length) {
          this.addFriend();
        }
        this.friendsWrapperElem.classList.add('mod-show');
      } else {
        this.friendsWrapperElem.classList.remove('mod-show');
      }
    });
  }

  addFriend() {
    const name = `friend-${ this.friendsGroups.length }`;
    const wrapper = document.createElement('div');
    wrapper.classList.add('ffg-host', `ffg-host-${ name }`);
    wrapper.innerHTML = `
      <div class="gl-form-inputs-list">
        <div class="gl-form-input-item">
          <label class="gl-input-wrapper friend-input-wrapper">
            <span class="gl-input-caption">Имя друга</span>
            <span class="input-wrapper">
              <input class="gl-input friend" type="text">
              <button type="button" class="btn-remove-nameFriend">x</button>
            </span>
          </label>
         </div>
      </div>
      <div class="ffg-error ffg-error-${ name }"></div>`
    const input = wrapper.querySelector('input');
    const btnRemove = wrapper.querySelector('button');
    this.friendsListElem.appendChild(wrapper);

    this.friendsGroups.push(
        new FieldsGroupClass(
            this.hostElemAsForm,
            name,
            input,
            {
              required: {
                param: true
              },
              minLength: {
                param: 3,
                message: 'Не менее 3-х символов'
              },
              maxLength: {
                param: 15,
                message: 'Не более 15-ти символов'
              }
            }
        )
    )

    btnRemove.onclick = () => {
      const group = this.friendsGroups.find(group => group.name === name);
      group.remove();
      this.friendsGroups = this.friendsGroups.filter(group => group.name !== name);

      if (!this.friendsGroups.length) {
        this.inputWithFriends.removeAttribute('checked');
        this.inputWithFriends.checked = false;
        this.toggleShowFriends();
      }
    }
  }

  validDoublePassword() {
    return this.repeatPasswordElem.value === this.passwordElem.value;
  }
}

window.onload = () => {
  window.addEventListener('click', e => {
    if (e.target.getAttribute('type') === 'checkbox') {
      if (e.target.getAttribute('checked')) {
        e.target.removeAttribute('checked');
        e.target.checked = false;
      } else {
        e.target.setAttribute('checked', true)
        e.target.checked = true;
      }
    }
  })

  new Page();
}
