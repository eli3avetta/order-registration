/*
constructor принимает поля
  formElem - враппер, содержащий в себе враппер группы
  name - атрибут name инпута(ов) группы
  group - HTML элемент/коллекция, содержащие в себе инпут/группу инпутов по name
  validations - содержит в себе объекты валидации.
    Существует:
    {
      required: {
        param: boolean,
        message: string // перекроет дефолтный текст ошибки
      },
    }
    {
      minLength: {
        param: number,
        message: string // перекроет дефолтный текст ошибки
      },
    }
    {
      maxLength: {
        param: number,
        message: string // перекроет дефолтный текст ошибки
      },
    }
    {
      minDate: {
        param: {
         date: new Date(год, месяц, день) -  конкретная тата
         или
         year:  -(number) - количество лет назад
         month:  -(number) - количество месяцев назад
         day:  -(number) - количество дней назад
         }
        message: string // перекроет дефолтный текст ошибки
      },
    }
    {
      maxDate: {
        param: {
         date: new Date(год, месяц, день) -  конкретная тата
         или
         year:  -(number) - количество лет назад
         month:  -(number) - количество месяцев назад
         day:  -(number) - количество дней назад
         }
        message: string // перекроет дефолтный текст ошибки
      },
    }
    {
      pattern: {
        param: регулярное выражение,
        message: string // перекроет дефолтный текст ошибки
      },
    }
    {
      custom: {
        param: function,
        message: string // перекроет дефолтный текст ошибки
      },
    }

  value - для установки изначального значения группы

пример HTML:
  <div class="ffg-host-GROUP_NAME"> // обязательный класс для врапера группы ffg-host и второй класс, где GROUP_NAME = name группы
    <label>
      <input type="checkbox" name="GROUP_NAME">
    </label>
    <div class="gl-field-error-GROUP_NAME"></div>
  </div>
*/

class FieldsGroupClass {
  formElem;
  hostElem;
  name;
  group;
  errorElem;
  validations;
  typeInput;
  isValidGroup = false;
  notValidArr = [];
  customError = '';

  constructor(formElem, name, group, validations, value) {
    this.formElem = formElem;
    this.hostElem = this.formElem.querySelector(`.ffg-host-${ name }`);
    this.name = name;

    this.group = group;
    this.validations = validations;
    this.errorElem = this.formElem.querySelector(`.ffg-error-${ name }`);

    if (this.group.length) {
      this.typeInput = this.group[0].getAttribute('type');
    } else {
      this.typeInput = this.group.getAttribute('type');
    }

    if (value) {
      this.setValue(value);
    }

    if (this.typeInput === 'radio' || this.typeInput === 'checkbox') {
      this.group.forEach(input => {
        input.onclick = () => {
          this.markAsShow();
        };
        input.oninput = () => {
          this.makeValidation();
        };
      })
    } else {
      this.group.onblur = () => {
        this.markAsShow();
      };
      this.group.oninput = () => {
        this.makeValidation();
      }
    }

    this.makeValidation();
  }

  markAsShow() {
    this.hostElem.classList.add('ffg-mod-show');
    if (this.errorElem) {
      this.errorElem.classList.add('ffg-mod-show');
    }
  }

  resetShow() {
    this.hostElem.classList.remove('ffg-mod-show');
    if (this.errorElem) {
      this.errorElem.classList.remove('ffg-mod-show');
    }
  }

  makeValidation() {
    this.isValidGroup = false;
    this.notValidArr = [];
    this.customError = '';
    if (this.validations) {
      Object.keys(this.validations).forEach(key => {
        switch (key) {
          case 'required':
            if (this.validations[key].param) {
              if (!this.checkExistValue()) {
                this.notValidArr.push(key)
              }
            }
            break;
          case 'minLength':
            if (this.typeInput === 'text' ||
                this.typeInput === 'tel' ||
                this.typeInput === 'email' ||
                this.typeInput === 'checkbox' ||
                this.typeInput === 'radio' ||
                this.typeInput === 'password') {
              if (this.validations[key].param) {
                if (this.checkExistValue()) {
                  if (!this.checkMinLength()) {
                    this.notValidArr.push(key);
                  }
                }
              }
            } else {
              console.error(`У типа групп ${ this.typeInput } не может быть валидации ${ key }`)
            }
            break;
          case 'maxLength':
            if (this.typeInput === 'text' ||
                this.typeInput === 'tel' ||
                this.typeInput === 'email' ||
                this.typeInput === 'checkbox' ||
                this.typeInput === 'radio' ||
                this.typeInput === 'password') {
              if (this.validations[key].param) {
                if (this.checkExistValue()) {
                  if (!this.checkMaxLength()) {
                    this.notValidArr.push(key);
                  }
                }
              }
            } else {
              console.error(`У типа групп ${ this.typeInput } не может быть валидации ${ key }`)
            }
            break;
          case 'pattern':
            if (this.typeInput === 'text' ||
                this.typeInput === 'tel' ||
                this.typeInput === 'email') {
              if (this.validations[key].param) {
                if (this.checkExistValue()) {
                  if (!this.checkPattern()) {
                    this.notValidArr.push(key);
                  }
                }
              }
            } else {
              console.error(`У типа групп ${ this.typeInput } не может быть валидации ${ key }`)
            }
            break;
          case 'minDate':
            if (this.typeInput === 'date') {
              if (this.validations[key]) {
                if (this.checkExistValue()) {
                  if (!this.checkMinDate()) {
                    this.notValidArr.push(key);
                  }
                }
              }
            } else {
              console.error(`У типа групп ${ this.typeInput } не может быть валидации ${ key }`)
            }
            break;
          case 'maxDate':
            if (this.typeInput === 'date') {
              if (this.validations[key]) {
                if (this.checkExistValue()) {
                  if (!this.checkMaxDate()) {
                    this.notValidArr.push(key);
                  }
                }
              }
            } else {
              console.error(`У типа групп ${ this.typeInput } не может быть валидации ${ key }`)
            }
            break;

          case 'custom':
            this.validations[key].forEach(customValidation => {
              if (!customValidation.param()) {
                this.customError = customValidation.message;
              }
            });
            break;

          default:
            console.error(`Не обработанная валидация: ${ key }`)
        }
      })
    }

    this.isValidGroup = !this.notValidArr.length && !this.customError;
    this.setStateError();
    return this.isValidGroup;
  }

  checkExistValue() {
    let exist = true;
    switch (true) {
      case this.typeInput === 'checkbox':
        const arr = [];
        this.group.forEach(elem => arr.push(elem.checked));
        if (!arr.some(b => b)) {
          exist = false;
        }
        break;

      case this.typeInput === 'text':
      case this.typeInput === 'date':
      case this.typeInput === 'tel':
      case this.typeInput === 'email':
      case this.typeInput === 'radio':
      case this.typeInput === 'password':
      case this.group.tagName && this.group.tagName.toLowerCase() === 'select':
        if (!this.group.value) {
          exist = false;
        }
        break;

      default:
        console.error(`Неизвестный тип: ${ this.typeInput }`)
    }

    return exist;
  }

  checkMinLength() {
    let validMinLength = true;
    switch (this.typeInput) {
      case 'checkbox':
        const arr = [];
        this.group.forEach(elem => {
          if (elem.checked) {
            arr.push(elem.checked)
          }
        });
        if (arr.length < this.validations.minLength.param) {
          validMinLength = false;
        }
        break;

      case 'text':
      case 'tel':
      case 'radio':
      case 'password':
        if (this.group.value.length < this.validations.minLength.param) {
          validMinLength = false;
        }
        break;

      default:
        console.error(`Неизвестный тип: ${ this.typeInput }`)
    }
    return validMinLength;
  }

  checkMaxLength() {
    let validMaxLength = true;
    switch (this.typeInput) {
      case 'checkbox':
        const arr = [];
        this.group.forEach(elem => {
          if (elem.checked) {
            arr.push(elem.checked)
          }
        });
        if (arr.length > this.validations.maxLength.param) {
          validMaxLength = false;
        }
        break;

      case 'text':
      case 'tel':
      case 'radio':
      case 'password':
        if (this.group.value.length > this.validations.maxLength.param) {
          validMaxLength = false;
        }
        break;

      default:
        console.error(`Неизвестный тип: ${ this.typeInput }`)
    }
    return validMaxLength;
  }

  checkMinDate() {
    let isValidMinDate = true;
    const selectDate = new Date(this.group.value);

    if (this.validations.minDate.param.date) {
      if (selectDate.getTime() < this.validations.minDate.param.date.getTime()) {
        isValidMinDate = false;
      }
    } else {
      const nowDate = new Date();
      const nowYear = nowDate.getFullYear()
      const nowMonth = nowDate.getMonth()
      const nowDay = nowDate.getDate()
      const minDate = new Date(
          nowYear + (this.validations.minDate.param.year || 0),
          nowMonth + 1 + (this.validations.minDate.param.month || 0),
          nowDay + (this.validations.minDate.param.day || 0)
      );
      if (selectDate.getTime() < minDate.getTime()) {
        isValidMinDate = false;
      }
    }

    return isValidMinDate;
  }

  checkMaxDate() {
    let isValidMaxDate = true;
    const selectDate = new Date(this.group.value);

    if (this.validations.maxDate.param.date) {
      if (selectDate.getTime() > this.validations.maxDate.param.date.getTime()) {
        isValidMaxDate = false;
      }
    } else {
      const nowDate = new Date();
      const maxYear = nowDate.getFullYear()
      const maxMonth = nowDate.getMonth()
      const maxDay = nowDate.getDate()
      const maxDate = new Date(
          maxYear + (this.validations.maxDate.param.year || 0),
          maxMonth + 1 + (this.validations.maxDate.param.month || 0),
          maxDay + (this.validations.maxDate.param.day || 0)
      );
      if (selectDate.getTime() > maxDate.getTime()) {
        isValidMaxDate = false;
      }
    }
    return isValidMaxDate;
  }

  checkPattern() {
    let validPattern = true;
    switch (this.typeInput) {
      case 'text':
      case 'email':
      case 'tel':
        if (this.group.length) {
          this.group.forEach(elem => {
            if (!elem.value.match(this.validations.pattern.param)) {
              validPattern = false;
            }
          })
        } else {
          if (!this.group.value.match(this.validations.pattern.param)) {
            validPattern = false;
          }
        }
    }
    return validPattern;
  }

  setStateError() {
    if (this.isValidGroup) {
      this.hostElem.classList.remove('ffg-mod-invalid');
      if (this.errorElem) {
        this.errorElem.classList.remove('ffg-mod-exist');
        this.errorElem.innerText = '';
      }
    } else {
      this.hostElem.classList.add('ffg-mod-invalid');
      if (this.errorElem) {
        this.errorElem.classList.add('ffg-mod-exist');
        this.notValidArr.forEach(key => {
          if (this.validations[key].message) {
            this.errorElem.innerText = this.validations[key].message;
          } else {
            switch (key) {
              case 'required':
                this.errorElem.innerText = 'Обязательно для заполнения';
                break;
              case 'minLength':
                this.errorElem.innerText = `Не менее ${ this.validations.minLength.param }`;
                break;
              case 'maxLength':
                this.errorElem.innerText = `Не менее ${ this.validations.maxLength.param }`;
                break;
              case 'minDate' || 'maxDate':
                this.errorElem.innerText = 'Неверная дата';
                break;
              case 'pattern':
                this.errorElem.innerText = 'Неверный формат';
                break;
            }
          }
        });
        if (this.customError) {
          this.errorElem.innerText = this.customError;
        }
      }
    }
  }

  remove() {
    this.hostElem.remove();
  }

  setValue(value) {
    if (this.typeInput === 'checkbox') {
      this.group.forEach(input => {
        if (value.includes(input.value)) {
          input.setAttribute('checked', true);
        }
      })
    } else {
      this.group.value = value;
    }
  }

  getValue() {
    if (this.typeInput === 'checkbox') {
      const arrCheckbox = [];
      this.group.forEach(input => {
        if (input.checked) {
          arrCheckbox.push(input.value)
        }
      })
      return arrCheckbox;
    } else {
      return this.group.value;
    }
  }
}
